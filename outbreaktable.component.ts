import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource, MatSort } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import { ApiService } from 'src/app/services/api.service';
import * as moment from 'moment-timezone';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { TravelerService } from 'src/app/services/traveler.service';
import * as _ from 'lodash';
import { FormGroup, FormControl, FormArray, ValidationErrors, Validators } from '@angular/forms';
import { RolesService } from 'src/app/services/roles.service';

@Component({
  selector: 'app-outbreak-table',
  templateUrl: './outbreak-table.component.html',
  styleUrls: ['./outbreak-table.component.scss']
})
export class OutbreakTableComponent {
  form: FormGroup = new FormGroup({});
  newform: FormGroup = new FormGroup({});
  outbreakData = [];
  isEditmode = false;
  editField: string;
  public user;
  displayedColumns: string[] = ['id', 'outbreakName', 'status'];
  controls: FormArray;

  ngOnInit() {
    this.loadData();
    this.newform.addControl('newoutbreakName', new FormControl(''));
    this.newform.get('newoutbreakName').updateValueAndValidity();
  }
  constructor(
    private changeDetector: ChangeDetectorRef,
    public dialog: MatDialog,
    private apiService: ApiService,
    private snackBar: SnackbarService,
    public travelerService: TravelerService,
    private rolesServ: RolesService
  ) {
    this.getCurrentUser();
  }

  public async getCurrentUser() {
    let email = this.rolesServ.getCurrentUserEmail();
    this.user = this.rolesServ.lookUpUsers({ email })[0];
  }

  async loadData(isPaginatorReset = false) {
    let apiMethod;
    this.form = new FormGroup({});

    // call api based on type
    apiMethod = await this.apiService.getOutbreakRecords();
    apiMethod.subscribe(
      (outbreak: any) => {
        this.outbreakData = _.cloneDeep(outbreak);

        this.form.addControl('outbreakName', new FormControl('', [Validators.required]));
        // this.form.patchValue({ outbreakName: this.outbreakData[0].outbreakName  });
        this.form.get('outbreakName').updateValueAndValidity();

        this.form.addControl('status', new FormControl('', [Validators.required]));
        // this.form.patchValue({ status: this.outbreakData[0].status  });
        this.form.get('status').updateValueAndValidity();
      },
      (error) => {
        this.snackBar.open('Error fetching data');
      }
    );
  }

  edit(i) {
    this.outbreakData = this.outbreakData.map((row) => {
      if (row.id == i + 1) {
        row.editable = true;
        this.form.patchValue({ outbreakName: row.outbreakName });
        this.form.patchValue({ status: row.status });
      } else {
        row.editable = false;
      }
      return row;
    });
  }

  aclRules(role) {
    if(this.user) {
      return this.user.role.includes(role);
    }
    return false;
  }

  async save(i) {
    let apiMethod;
    let toupdate = {};
    toupdate['outbreakName'] = this.form.get('outbreakName').value;

    toupdate['status'] = this.form.get('status').value;

    apiMethod = await this.apiService.updateOutbreak(i + 1, toupdate);
    apiMethod.subscribe((result: any) => {
      this.snackBar.open('Record successfully edited and saved');
      this.ngOnInit();
    });
    this.outbreakData = this.outbreakData.map((row) => {
      if (row.id == i + 1) {
        row.editable = false;
      }
      return row;
    });
  }
  async cancel(i) {
    this.outbreakData = this.outbreakData.map((row) => {
      if (row.id == i + 1) {
        row.editable = false;
      }
      return row;
    });
  }

  async add() {
    let apiMethod;

    if (this.newform.get('newoutbreakName').valid) {
      let toupdate = {};

      toupdate['outbreakName'] = this.newform.get('newoutbreakName').value;
      toupdate['status'] = true;
      toupdate['createdAt'] = moment().toDate();
      toupdate['modifiedAt'] = moment().toDate();

      apiMethod = await this.apiService.addOutbreakRecords(toupdate);
      apiMethod.subscribe((result) => {
        this.ngOnInit();
        this.snackBar.open('Record successfully saved');
        this.newform.get('newoutbreakName').setValue('');
      });
    }
  }

  async changeValue(id: number, property: string, event: any) {
    if (property == 'outcomeName') {
      this.editField = event.target.textContent;
    } else {
      this.editField = event.target.checked;
    }
  }
}
